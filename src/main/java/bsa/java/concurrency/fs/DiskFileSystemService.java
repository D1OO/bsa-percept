package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class DiskFileSystemService implements FileSystem {
    @Value("${storage.dir}")
    private String storageDir;
    @Value("${api-url}")
    private String apiUrl;

    @Override
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<String> saveFile(Resource resource) throws IOException {
        return CompletableFuture.completedFuture(saveFileToDisk(resource));
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<Void> delete(String fileName) {
        Path deletePath = Path.of(String.format("%s/%s", storageDir, StringUtils.cleanPath(fileName)));
        deletePath.toFile().delete();
        return CompletableFuture.completedFuture(null);
    }

    @Override
    @Async("threadPoolTaskExecutor")
    public CompletableFuture<Void> purge() throws IOException {
        Path purgePath = Path.of(storageDir);
        if (purgePath.toFile().exists()) {
            Files.walk(purgePath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        purgePath.toFile().mkdirs();
        return CompletableFuture.completedFuture(null);
    }

    public String saveFileToDisk(Resource resource) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(resource.getFilename()));
        Path saveToPath = Path.of(String.format("%s/%s", storageDir, fileName));
        try (InputStream is = resource.getInputStream()) {
            Files.copy(is, saveToPath, StandardCopyOption.REPLACE_EXISTING);
        }
        return formResourceUrl(fileName);
    }

    private String formResourceUrl(String resourceName) {
        return String.format("%s/%s", apiUrl, resourceName);
    }

}

package bsa.java.concurrency.fs;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface FileSystem {

    CompletableFuture<String> saveFile(Resource resource) throws IOException;

    CompletableFuture<Void> delete(String fileName);

    CompletableFuture<Void> purge() throws IOException;

}

package bsa.java.concurrency.image.service;

import bsa.java.concurrency.image.entity.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class ImageProcessingService {
    @Autowired
    ImageHashService imageHashService;
    @Autowired
    ImageService imageService;
    @Autowired
    ExecutorService executorService;

    public void batchProcessThenPersist(MultipartFile[] files) {
        try {
            batchProcessThenPersistFuture(files).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private Future<?> batchProcessThenPersistFuture(MultipartFile[] files) {
        return executorService.submit(() ->
                Arrays.stream(files)
                        .parallel()
                        .map(MultipartFile::getResource)
                        .forEach(this::processFile));
    }

    private void processFile(Resource resource) {
        Image i;
        try {
            CompletableFuture<String> savedName = imageService.saveResource(resource);
            long hash = imageHashService.calculateHash(resource);

            i = new Image(hash, savedName.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        imageService.save(i);
    }

}

package bsa.java.concurrency.image.service;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.ImageRepository;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 16.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class ImageService {
    @Autowired
    FileSystem fileSystem;
    @Autowired
    ImageRepository imageRepository;
    @Autowired
    ImageHashService imageHashService;

    public Map.Entry<Long, List<SearchResultDTO>> search(MultipartFile image, double threshold) {
        long hash = imageHashService.calculateHash(image.getResource());
        return new AbstractMap.SimpleEntry<>(
                hash,
                imageRepository.findSimilar(hash, threshold));
    }

    @Async("threadPoolTaskExecutor")
    public void createFromResource(long hash, Resource resource)
            throws InterruptedException, ExecutionException, IOException {

        String resourceUrl = this.saveResource(resource).get();
        this.save(new Image(hash, resourceUrl));
    }

    public CompletableFuture<String> saveResource(Resource resource) throws IOException {
        return fileSystem.saveFile(resource);
    }

    public void save(Image image) {
        imageRepository.save(image);
    }

    public void deleteById(UUID id) throws MalformedURLException {
        Optional<Image> i = imageRepository.findById(id);
        if (i.isPresent()) {
            String fileName = new URL(i.get().getImageUrl()).getPath();
            fileSystem.delete(fileName);
            imageRepository.deleteById(id);
        }
    }

    public void purgeImages() throws IOException {
        fileSystem.purge();
        imageRepository.deleteAll();
    }
}

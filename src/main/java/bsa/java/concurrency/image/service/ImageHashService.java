package bsa.java.concurrency.image.service;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public class ImageHashService {

    public long calculateHash(Resource image) {
        try (InputStream is = image.getInputStream()) {
            return calculateDHash(preprocessImage(ImageIO.read(is)));
        } catch (Exception err) {
            throw new RuntimeException(err);
        }
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (var row = 1; row < 8; row++) {
            for (var col = 1; col < 9; col++) {
                var prev = brightnessScore(processedImage.getRGB(col - 1, row - 1));
                var current = brightnessScore(processedImage.getRGB(col, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

    private static BufferedImage preprocessImage(BufferedImage image) {
        long m = System.currentTimeMillis();
        var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var output = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);
        return output;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

}

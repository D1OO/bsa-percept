package bsa.java.concurrency.image.exception;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@ControllerAdvice
@Slf4j
public class Handler {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private Map<String, Object> handleValidationException(ConstraintViolationException e) {
        log.warn("Request load not valid: " + e);

        return e.getConstraintViolations().stream()
                .collect(Collectors.toMap(
                        fieldErr -> ((PathImpl) fieldErr.getPropertyPath()).getLeafNode().asString(),
                        ConstraintViolation::getInvalidValue));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private Map.Entry<String, Object> handleTypeMismatchException(MethodArgumentTypeMismatchException e) {
        log.warn("Request load - type mismatch: " + e);

        return new AbstractMap.SimpleEntry<>(e.getName(), e.getValue());
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private void handleMultipartAbsent(MissingServletRequestPartException e) {
        log.warn("Multipart is absent: " + e);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
    private void handleMaxSizeExceeded(MaxUploadSizeExceededException e) {
        log.warn("Multipart size exceeded: " + e);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private void handleExceptions(Exception e) {
        logStackTrace(e);
    }

    private void logStackTrace(Exception e) {
        StringWriter errorStackTrace = new StringWriter();
        e.printStackTrace(new PrintWriter(errorStackTrace));
        log.error("\n\n" + errorStackTrace.toString());
    }
}

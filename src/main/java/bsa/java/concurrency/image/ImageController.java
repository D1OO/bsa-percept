package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.service.ImageProcessingService;
import bsa.java.concurrency.image.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@Validated
@RequestMapping("/image")
public class ImageController {
    @Autowired
    ImageProcessingService imageProcessingService;
    @Autowired
    ImageService imageService;

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") @NotEmpty MultipartFile[] files) {
        imageProcessingService.batchProcessThenPersist(files);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image")
                                                       MultipartFile file,
                                               @RequestParam(defaultValue = "0.9")
                                               @DecimalMin("0.1")
                                               @DecimalMax("1.0")
                                                       double threshold)
            throws IOException, ExecutionException, InterruptedException {

        Map.Entry<Long, List<SearchResultDTO>> similarToHash = imageService.search(file, threshold);
        if (similarToHash.getValue().isEmpty()) {
            imageService.createFromResource(similarToHash.getKey(), file.getResource());
        }
        return similarToHash.getValue();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) throws MalformedURLException {
        imageService.deleteById(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() throws IOException {
        imageService.purgeImages();
    }

}

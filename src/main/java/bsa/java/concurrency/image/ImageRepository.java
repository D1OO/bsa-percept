package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Service
public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(nativeQuery = true, value = "SELECT cast(i.id as varchar) imageId, i.image_url imageurl, i.mp matchpercent " +
            " FROM (select id, image_url, match_index(hash, :hash) mp FROM image) i" +
            " WHERE i.mp >= :threshold ")
    List<SearchResultDTO> findSimilar(@Param("hash") long hash, @Param("threshold") double threshold);

}

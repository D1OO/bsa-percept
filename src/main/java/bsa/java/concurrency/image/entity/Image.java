package bsa.java.concurrency.image.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * 14.07.2020
 *
 * @author Dmitriy Storozhenko
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "image")
public class Image {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    UUID imageId;
    long hash;
    String imageUrl;

    public Image(long hash, String imageUrl) {
        this.hash = hash;
        this.imageUrl = imageUrl;
    }
}

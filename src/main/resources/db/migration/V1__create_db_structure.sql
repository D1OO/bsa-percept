create table image
(
    id uuid not null,
    hash      bigint,
    image_url varchar(255),
    primary key (id)
);
CREATE OR REPLACE FUNCTION bit_count(initial_num BIGINT) RETURNS INT AS
$$
BEGIN
    DECLARE
        msb INT;
        num bit(64);
    BEGIN
        msb = 0;
        num = initial_num::bit(64);
        IF initial_num < 0 THEN
            msb = 1;
            num = B'0111111111111111111111111111111111111111111111111111111111111111' & num;
        END IF;

        return array_length(string_to_array(num::varchar, '1'), 1) - 1 + msb;
    END;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION match_index(p1 bigint, p2 bigint) RETURNS double precision AS
$$
BEGIN
    return (1 - cast(bit_count(p1 # p2) as double precision) / 64);
END;
$$ LANGUAGE plpgsql;